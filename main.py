from PIL import Image, ImageDraw, ImageFont

def add_text_to_image(input_image_path, output_image_path, text, position=(10, 10), font_size=20, font_path=None):
    # 打开图片
    image = Image.open(input_image_path)

  # 获取图片的宽度和高度
    width, height = image.size
    
    # 创建一个可以在图片上绘图的对象
    draw = ImageDraw.Draw(image)
    
    # 选择字体和大小
    if font_path:
        font = ImageFont.truetype('fzhtjt.ttf', font_size, encoding="unic")
    else:
        font = ImageFont.truetype('fzhtjt.ttf', font_size, encoding="unic")
        # font = ImageFont.load_default()  # 使用默认字体
    
    text_width, text_height = draw.textsize(text, font)

     # 计算文字在图片中心的位置
    x = (width - text_width) // 2
    y = (height - text_height) // 2
    
    # 在指定位置添加文字
    draw.text((x,y), text, font=font, fill=255)
    
    
    # 保存修改后的图片
    image.save(output_image_path)

# 使用示例
input_path = "bg.jpeg"
output_path = "output_image_with_text.jpg"
text_to_add = "将从系统中清除。"

# 注意：在这个例子中，我使用了整数表示的颜色
font_color = (255, 255, 255)

output_str = "白内障人群发病年龄多见于、40岁以上，其中60-8，9岁人群中，白内障发病率高、达80%" 
strs = output_str.split('、')
for s in strs:
    oPath =  './output/' + s + '.jpg'
    add_text_to_image(input_path, output_path, s)
    print(s,oPath, '=============')

# add_text_to_image(input_path, output_path, text_to_add)
